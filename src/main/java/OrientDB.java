import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;

import javax.annotation.PostConstruct;

/**
 * Created by szwedi on 30.09.2016.
 */
public class OrientDB {

    private static OrientDB instance = null;
    private OrientGraphFactory orientGraphFactory;
    private FramedGraphFactory framedGraphFactory;

    public static OrientDB getInstance() {
        if(instance == null) {
            instance = new OrientDB();
        }
        return instance;
    }

    public void init(){
        orientGraphFactory = new OrientGraphFactory("remote:localhost/OpenBeer", "admin", "admin");
        orientGraphFactory.setupPool(1, 2);

        framedGraphFactory = new FramedGraphFactory(new GremlinGroovyModule());
    }

    public void execute(GraphOperation graphOperation){
        FramedGraph framedGraph = framedGraphFactory.create(orientGraphFactory.getNoTx());
        try{
            graphOperation.execute(framedGraph);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            framedGraph.shutdown();
        }
    }

//    public void execute(GraphOperation graphOperation){
//        OrientGraphNoTx orientGraphNoTx = orientGraphFactory.getNoTx();
//        FramedGraph framedGraph = framedGraphFactory.create(orientGraphNoTx);
//        try{
//            graphOperation.execute(framedGraph);
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }finally {
//            orientGraphNoTx.shutdown();
//        }
//    }

    public Iterable<CategoryVertex> getCategories(FramedGraph framedGraph){
        Iterable<CategoryVertex> categoryVertexIterable = framedGraph.query().has("@class","Category").vertices(CategoryVertex.class);

        return categoryVertexIterable;
    }
}
