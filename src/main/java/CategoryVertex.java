import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import java.util.Date;

/**
 * Created by szwedi on 13.04.2016.
 */
public interface CategoryVertex extends VertexFrame {

    @Property("id")
    Integer getId();

    @Property("id")
    void setId(Integer id);

    @Property("cat_name")
    String getName();

    @Property("cat_name")
    void setName(String name);

    @Property("last_mod")
    Date getLastMod();

    @Property("last_mod")
    void setLastMod(Date lastMod);
}
