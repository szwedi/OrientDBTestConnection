import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;


import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by szwedi on 30.09.2016.
 */
public class Application {

    public static void main(String [] args) throws IOException {
        OrientDB orientDB = OrientDB.getInstance();
        orientDB.init();


        Runnable[] runners = new Runnable[5];
        Thread[] threads = new Thread[5];

        runners[0] = new MyRun(1, 1000);
        runners[1] = new MyRun(2, 200);
        runners[2] = new MyRun(3, 500);
        runners[3] = new MyRun(4, 100);
        runners[4] = new MyRun(5, 2000);


        for(int i=0; i<5; i++) {
            threads[i] = new Thread(runners[i]);
        }

        for(int i=0; i<5; i++) {
            threads[i].start();
        }

        int read = System.in.read();
        System.out.println(read);
    }
}
