import com.tinkerpop.frames.FramedGraph;

public interface GraphOperation {
    void execute(FramedGraph framedGraph) throws Exception;
}