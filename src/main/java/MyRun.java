/**
 * Created by szwedi on 30.09.2016.
 */
public class MyRun implements Runnable {

    private int id;
    private long sleep;

    public MyRun(int id, long sleep) {
        this.id = id;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        OrientDB orientDB = OrientDB.getInstance();

        orientDB.execute(framedGraph -> {
            Thread.sleep(sleep);
            Iterable<CategoryVertex> categoryVertexIterable = orientDB.getCategories(framedGraph);

            categoryVertexIterable.forEach(categoryVertex -> {
                System.out.println("Category id: " + categoryVertex.getId() +  " Category name: " + categoryVertex.getName() + " On thread " + id);
            });
        });

        System.out.println("End working on thread " + id);
    }
}
